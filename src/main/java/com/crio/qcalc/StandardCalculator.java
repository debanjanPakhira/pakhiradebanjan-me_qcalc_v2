package com.crio.qcalc;

public class StandardCalculator {
    protected double result;
    public static void getVersion(){
        System.out.println("Standard Calculator 1.0");
    }

    public final void add(double num1, double num2){
        result=(num1+ num2);
        if((result == Double.MAX_VALUE) || (result == Double.POSITIVE_INFINITY)){

            throw new ArithmeticException("Double overflow");
        }
        
    }



public final void subtract(double num1, double num2){
    result =(num1 - num2);
    if((result==-Double.MAX_VALUE)||(result==Double.NEGATIVE_INFINITY)){
        throw new ArithmeticException("Double overflow");
        
    }
}


public final void multiply(double num1, double num2){
    result = (num1 * num2);
    if((result==-Double.MAX_VALUE)||(result==Double.NEGATIVE_INFINITY)||(result==Double.POSITIVE_INFINITY)){
        throw new ArithmeticException("Double overflow");
    }
}


public final void divide(double num1, double num2){
    if(num2==0){
        throw new ArithmeticException("Divide By Zero");

    }
    result = (num1 / num2);
}
public double getResult() {

    return result;

}
public void clearResult() {

    result = 0;

}
public void printResult(){

    System.out.println("Standard Calculator Result:"+ result);

}



}
